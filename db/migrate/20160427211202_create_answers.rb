class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :name, null: false
      t.boolean :correct, null: false, default: false
      t.references :question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
