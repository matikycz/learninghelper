class Question < ActiveRecord::Base
  belongs_to :type
  belongs_to :test
  has_many :answers, dependent: :destroy

  validates_presence_of :name, :type, :test
end
