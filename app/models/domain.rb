class Domain < ActiveRecord::Base
  has_many :tests

  validates_presence_of :name
  validates_uniqueness_of :name
end
