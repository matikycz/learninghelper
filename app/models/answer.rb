class Answer < ActiveRecord::Base
  belongs_to :question

  validates_presence_of :name, :question
  validates_inclusion_of :correct, in: [true, false]
end
