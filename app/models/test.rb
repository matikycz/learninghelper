class Test < ActiveRecord::Base
  belongs_to :domain
  has_many :questions

  validates_presence_of :name, :domain
end
