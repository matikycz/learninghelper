class V1::AnswerRepresenter
  def initialize(answer)
    @answer = answer
  end

  def basic
    {
      id: @answer.id,
      name: @answer.name,
      correct: @answer.correct
    }
  end
end
