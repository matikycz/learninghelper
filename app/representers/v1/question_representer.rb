class V1::QuestionRepresenter
  def initialize(question)
    @question = question
  end

  def basic
    {
      id: @question.id,
      name: @question.name,
      type: @question.type_id,
      answers: @question.answers.map{|answer| ::V1::AnswerRepresenter.new(answer).basic}
    }
  end
end
