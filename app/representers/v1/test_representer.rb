class V1::TestRepresenter
  def initialize(test)
    @test = test
  end

  def basic
    {
      id: @test.id,
      name: @test.name,
      description: @test.description,
      domain_id: @test.domain_id,
      questions: @test.questions.map{|question| ::V1::QuestionRepresenter.new(question).basic}
    }
  end

  def short
    {
      id: @test.id,
      name: @test.name,
      description: @test.description
    }
  end
end
