class V1::TypeRepresenter
  def initialize(type)
    @type = type
  end

  def basic
    {
      id: @type.id,
      name: @type.name
    }
  end
end
