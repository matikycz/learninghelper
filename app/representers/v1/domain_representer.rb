class V1::DomainRepresenter
  def initialize(domain)
    @domain = domain
  end

  def basic
    {
      id: @domain.id,
      name: @domain.name,
      tests: @domain.tests.map{|test| ::V1::TestRepresenter.new(test).short}
    }
  end
end
