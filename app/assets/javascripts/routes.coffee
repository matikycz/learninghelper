angular.module('learninghelper').config ($stateProvider, $urlRouterProvider, $locationProvider) ->
	$locationProvider.html5Mode true
	$urlRouterProvider.otherwise '/'

	rootPath = '/assets/'

	$stateProvider
		.state 'root',
			abstract: true
			template: '<ui-view></ui-view>'
		.state 'root.index',
			url: '/'
			templateUrl: rootPath + 'index.html'
			controller: 'HomeController'
			resolve:
				domains: (Domains) ->
					Domains.index().then (response) -> response.data
		.state 'root.tests',
			url: '/domains/:id/tests'
			templateUrl: rootPath + 'tests/index.html'
			controller: 'DomainsController'
			resolve:
				domain: ($stateParams, Domains) ->
					Domains.show($stateParams.id).then (response) -> response.data
		.state 'root.test',
			url: '/test/:id'
			templateUrl: rootPath + 'tests/show.html'
			controller: 'TestsController'
			resolve:
				test: ($stateParams, Tests) ->
					Tests.show($stateParams.id).then (response) -> response.data
		.state 'root.adminpanel',
			abstract: true
			template: '<ui-view></ui-view>'
		.state 'root.adminpanel.index',
			url: '/adminpanel'
			templateUrl: rootPath + 'adminpanel/index.html'
		.state 'root.adminpanel.domains',
			abstract: true
			template: '<ui-view></ui-view>'
		.state 'root.adminpanel.domains.index',
			url: '/adminpanel/domains'
			templateUrl: rootPath + 'adminpanel/domains/index.html'
			controller: 'AdminPanelDomainsController'
			resolve:
				domains: (Domains) ->
					Domains.index().then (response) -> response.data
		.state 'root.adminpanel.tests',
			abstract: true
			template: '<ui-view></ui-view>'
		.state 'root.adminpanel.tests.index',
			url: '/adminpanel/tests'
			templateUrl: rootPath + 'adminpanel/tests/index.html'
			controller: 'AdminPanelTestsController'
			resolve:
				tests: (Tests) ->
					Tests.index().then (response) -> response.data
		.state 'root.adminpanel.tests.new',
			url: '/adminpanel/tests/new'
			templateUrl: rootPath + 'adminpanel/tests/new.html'
			controller: 'AdminPanelNewTestsController'
		.state 'root.adminpanel.tests.edit',
			url: '/adminpanel/tests/:id/edit'
			templateUrl: rootPath + 'adminpanel/tests/edit.html'
			controller: 'AdminPanelEditTestsController'
			resolve:
				test: ($stateParams, Tests) ->
					Tests.show($stateParams.id).then (response) -> response.data
