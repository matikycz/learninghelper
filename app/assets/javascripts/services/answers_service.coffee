angular.module('learninghelper').factory 'Answers', ($http) ->
	base = 'api/v1/tests/'

	index: (testId, questionId) -> $http.get(base + testId + '/questions/' + questionId + '/answers')
	show: (testId, questionId, id) -> $http.get(base + testId + '/questions/' + questionId + '/answers/' + id)
	create: (testId, questionId, params) -> $http.post(base + testId + '/questions/' + questionId + '/answers', params)
	update: (testId, questionId, id, params) -> $http.put(base + testId + '/questions/' + questionId + '/answers/' + id, params)
	destroy: (testId, questionId, id) -> $http.delete(base + testId + '/questions/' + questionId + '/answers/' + id)
