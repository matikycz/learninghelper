angular.module('learninghelper').factory 'Domains', ($http) ->
	base = 'api/v1/domains'

	index: -> $http.get(base)
	show: (id) -> $http.get(base + '/' + id)
	create: (params) -> $http.post(base, params)
	update: (params, id) -> $http.put(base + '/' + id, params)
	destroy: (id) -> $http.delete(base + '/' + id)
