angular.module('learninghelper').factory 'Questions', ($http) ->
	base = 'api/v1/tests/'

	index: (testId) -> $http.get(base + testId + '/questions')
	show: (testId, id) -> $http.get(base + testId + '/questions/' + id)
	create: (testId, params) -> $http.post(base + testId + '/questions', params)
	update: (testId, id, params) -> $http.put(base + testId + '/questions/' + id, params)
	destroy: (testId, id) -> $http.delete(base + testId + '/questions/' + id)
