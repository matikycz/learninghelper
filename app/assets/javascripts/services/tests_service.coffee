angular.module('learninghelper').factory 'Tests', ($http) ->
	base = 'api/v1/tests'

	index: -> $http.get(base)
	show: (id) -> $http.get(base + '/' + id)
	create: (params) -> $http.post(base, params)
	update: (params, id) -> $http.put(base + '/' + id, params)
	destroy: (id) -> $http.delete(base + '/' + id)
