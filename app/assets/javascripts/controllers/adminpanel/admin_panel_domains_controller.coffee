angular.module('learninghelper').controller 'AdminPanelDomainsController', ($scope, domains, Domains) ->
	$scope.domains = domains
	$scope.success = false

	$scope.createOrUpdate = (id) ->
		domain = $scope.domains[id]
		$scope.success = false
		$scope.errors = null
		if domain.id
			Domains.update(domain, domain.id).then ((response) ->
				$scope.success = true
			), (error) ->
				$scope.errors = error.data
		else
			Domains.create(domain).then ((response) ->
				$scope.domains[id].id = response.data.id
				$scope.success = true
				init()
			), (error) ->
				$scope.errors = error.data

	$scope.delete = (id) ->
		domain = $scope.domains[id]
		$scope.errors = null
		$scope.success = false
		Domains.destroy(domain.id).then ((response) ->
			$scope.success = true
			$scope.domains.splice(id, 1)
		), (error) ->
			$scope.errors = error.data

	init = ->
		$scope.domains.push {name: ''}

	init()
