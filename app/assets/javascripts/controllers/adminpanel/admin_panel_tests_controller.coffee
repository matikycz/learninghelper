angular.module('learninghelper').controller 'AdminPanelTestsController', ($scope, $state, tests, Tests) ->
	$scope.tests = tests
	$scope.success = false

	$scope.delete = (id) ->
		test = $scope.tests[id]
		$scope.errors = null
		$scope.success = false
		Tests.destroy(test.id).then ((response) ->
			$scope.success = true
			$scope.tests.splice(id, 1)
		), (error) ->
			$scope.errors = error.data
