angular.module('learninghelper').controller 'HomeController', ($scope, $state, domains) ->
	$scope.domains = domains

	$scope.chooseDomain = (domain_id) ->
		$state.go 'root.tests', {id: domain_id}
