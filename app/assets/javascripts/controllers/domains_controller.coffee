angular.module('learninghelper').controller 'DomainsController', ($scope, $state, domain) ->
	$scope.domain = domain

	$scope.chooseTest = (test_id) ->
		$state.go 'root.test', {id: test_id}
