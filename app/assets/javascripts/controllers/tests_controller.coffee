angular.module('learninghelper').controller 'TestsController', ($scope, $state, $stateParams, test) ->
	$scope.test = test
	$scope.toLearn = test.questions
	$scope.learned = []
	$scope.questionsCount = test.questions.length
	$scope.correctAnswers = 0
	$scope.incorrectAnswers = 0
	$scope.progress = 0

	$scope.parameters = {initCount: 3, errorCount: 3}

	$scope.start = true
	$scope.answered = false
	$scope.end = false

	$scope.startTest = ->
		if Number.isInteger($scope.parameters.initCount) && Number.isInteger($scope.parameters.errorCount)
			for question in $scope.toLearn
				question.repeat = $scope.parameters.initCount
			$scope.start = false
			init()

	$scope.showAnswer = ->
		$scope.answered = true

	$scope.correctAnswer = ->
		$scope.correctAnswers++
		$scope.answered = false
		$scope.question.repeat--
		if $scope.question.repeat == 0
			$scope.toLearn.splice($scope.index, 1)
			$scope.learned.push $scope.question
		if $scope.toLearn.length > 0
			init()
		else
			$scope.end = true

	$scope.incorrectAnswer = ->
		$scope.incorrectAnswers++
		$scope.answered = false
		$scope.question.repeat += $scope.parameters.errorCount
		init()

	$scope.repeatTest = ->
		$state.go $state.current, {id: $stateParams.id}, {reload: true}
	
	$scope.backToHome = ->
		$state.go 'root.index'

	randomQuestion = ->
		$scope.index = Math.floor(Math.random() * $scope.toLearn.length)
		$scope.toLearn[$scope.index]

	init = ->
		$scope.question = randomQuestion()
		$scope.progress = $scope.learned.length*100/$scope.questionsCount
