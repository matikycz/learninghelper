module Api
  module V1
    class AnswersController < Api::V1::BaseController
      def index
        begin
          answers = Answer.includes(:question).where(questions: {id: params[:question_id], test_id: params[:test_id]})
          answers = answers.map do |answer|
            ::V1::AnswerRepresenter.new(answer).basic
          end
          render json: answers
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question not found']}, status: 404
        end
      end

      def show
        begin
          answer = Answer.includes(:question).where(questions: {id: params[:question_id], test_id: params[:test_id]}).find(params[:id])
          if answer.present?
            render json: ::V1::AnswerRepresenter.new(answer).basic, status: 200
          else
            render json: {errors: ['Test or question or answer not found']}, status: 404
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question or answer not found']}, status: 404
        end
      end

      def create
        answer = Answer.new(answer_params)
        if answer.save
          render json: ::V1::AnswerRepresenter.new(answer).basic, status: 201
        else
          render json: {errors: answer.errors}, status: 422
        end
      end

      def update
        begin
          answer = Answer.includes(:question).where(questions: {id: params[:question_id], test_id: params[:test_id]}).find(params[:id])
          if answer.update(answer_params)
            render json: ::V1::AnswerRepresenter.new(answer).basic, status: 200
          else
            render json: {errors: answer.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question or answer not found']}, status: 404
        end
      end

      def destroy
        begin
          answer = Answer.includes(:question).where(questions: {id: params[:question_id], test_id: params[:test_id]}).find(params[:id])
          if answer.destroy
            render json: ::V1::AnswerRepresenter.new(answer).basic, status: 200
          else
            render json: {errors: answer.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question or answer not found']}, status: 404
        end
      end

      private
      def answer_params
        params.permit(:name, :correct, :question_id)
      end
    end
  end
end
