module Api
  module V1
    class TestsController < Api::V1::BaseController
      def index
        tests = Test.includes(questions: :answers)
        tests = tests.map do |test|
          ::V1::TestRepresenter.new(test).basic
        end
        render json: tests
      end

      def show
        begin
          test = Test.includes(questions: :answers).find(params[:id])
          render json: ::V1::TestRepresenter.new(test).basic, status: 200
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test not found']}, status: 404
        end
      end

      def create
        test = Test.new(test_params)
        if test.save
          render json: ::V1::TestRepresenter.new(test).basic, status: 201
        else
          render json: {errors: test.errors}, status: 422
        end
      end

      def update
        begin
          test = Test.includes(questions: :answers).find(params[:id])
          if test.update(test_params)
            render json: ::V1::TestRepresenter.new(test).basic, status: 200
          else
            render json: {errors: test.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test not found']}, status: 404
        end
      end

      def destroy
        begin
          test = Test.includes(questions: :answers).find(params[:id])
          if test.destroy
            render json: ::V1::TestRepresenter.new(test).basic, status: 200
          else
            render json: {errors: test.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test not found']}, status: 404
        rescue ActiveRecord::StatementInvalid
          render json: {errors: ['Unable to destroy test with questions']}, status: 405
        end
      end

      private
      def test_params
        params.permit(:name, :description, :domain_id)
      end
    end
  end
end
