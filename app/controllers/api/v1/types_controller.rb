module Api
  module V1
    class TypesController < Api::V1::BaseController
      def index
        types = Type.all
        types = types.map do |type|
          ::V1::TypeRepresenter.new(type).basic
        end
        render json: types
      end
    end
  end
end
