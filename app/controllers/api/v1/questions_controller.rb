module Api
  module V1
    class QuestionsController < Api::V1::BaseController
      def index
        begin
          questions = Question.where(test_id: params[:test_id]).includes(:answers)
          questions = questions.map do |question|
            ::V1::QuestionRepresenter.new(question).basic
          end
          render json: questions
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test not found']}, status: 404
        end
      end

      def show
        begin
          question = Question.includes(:answers).where(id: params[:id]).where(test_id: params[:test_id]).first
          if question.present?
            render json: ::V1::QuestionRepresenter.new(question).basic, status: 200
          else
            render json: {errors: ['Test or question not found']}, status: 404
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question not found']}, status: 404
        end
      end

      def create
        question = Question.new(question_params)
        if question.save
          render json: ::V1::QuestionRepresenter.new(question).basic, status: 201
        else
          render json: {errors: question.errors}, status: 422
        end
      end

      def update
        begin
          question = Question.includes(:answers).find(params[:id])
          if question.update(question_params)
            render json: ::V1::QuestionRepresenter.new(question).basic, status: 200
          else
            render json: {errors: question.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question not found']}, status: 404
        end
      end

      def destroy
        begin
          question = Question.includes(:answers).find(params[:id])
          if question.destroy
            render json: ::V1::QuestionRepresenter.new(question).basic, status: 200
          else
            render json: {errors: question.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Test or question not found']}, status: 404
        end
      end

      private
      def question_params
        params.permit(:name, :type_id, :test_id)
      end
    end
  end
end
