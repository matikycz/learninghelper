module Api
  module V1
    class DomainsController < Api::V1::BaseController
      def index
        domains = Domain.includes(:tests).all
        domains = domains.map do |domain|
          ::V1::DomainRepresenter.new(domain).basic
        end
        render json: domains
      end

      def show
        begin
          domain = Domain.includes(:tests).find(params[:id])
          render json: ::V1::DomainRepresenter.new(domain).basic, status: 201
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Domain not found']}, status: 404
        end
      end

      def create
        domain = Domain.new(domain_params)
        if domain.save
          render json: ::V1::DomainRepresenter.new(domain).basic, status: 201
        else
          render json: {errors: domain.errors}, status: 422
        end
      end

      def update
        begin
          domain = Domain.includes(:tests).find(params[:id])
          if domain.update(domain_params)
            render json: ::V1::DomainRepresenter.new(domain).basic, status: 200
          else
            render json: {errors: domain.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Domain not found']}, status: 404
        end
      end

      def destroy
        begin
          domain = Domain.includes(:tests).find(params[:id])
          if domain.destroy
            render json: ::V1::DomainRepresenter.new(domain).basic, status: 200
          else
            render json: {errors: domain.errors}, status: 422
          end
        rescue ActiveRecord::RecordNotFound
          render json: {errors: ['Domain not found']}, status: 404
        rescue ActiveRecord::StatementInvalid
          render json: {errors: ['Unable to destroy domain with test']}, status: 405
        end
      end

      private
      def domain_params
        params.permit(:name)
      end
    end
  end
end
