# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :answer do
    name "MyString"
    correct false
    question nil
  end
end
